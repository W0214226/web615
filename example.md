# Web 615 Midterm Notes #

#### Notes

* Some demonstrative systems, precursory to autonomous cars, date back to the 1920s and 30s. The first self-sufficient cars appeared in the 1980s
* Autonomous vehicles feel their surroundings with such techniques as radar, lidar, GPS, Odometry, and computer vision.
* numerous major companies and research organizations have developed working prototype autonomous vehicles, including Mercedes-Benz, General Motors, Continental Automotive Systems, IAV, Autoliv Inc., Bosch, Nissan, Renault, Toyota, Audi, Volvo, Tesla Motors, Peugeot, AKKA Technologies, Vislab from University of Parma, Oxford University and Google.
* In Europe, cities in Belgium, France, Italy and the UK are planning to operate transport systems for driverless cars and Germany, the Netherlands, and Spain have allowed testing robotic cars in traffic.
* Public opinion surveys for 2015 are more or less divided evenly between good and bad.

#### Links

* [Rand](http://www.rand.org/pubs/research_reports/RR443-1.html)
* [6 firms that are testing driverless cars](http://www.bankrate.com/finance/auto/companies-testing-driverless-cars-1.aspx)
* [History](https://en.wikipedia.org/wiki/History_of_autonomous_car)
* [wikipedia](https://en.wikipedia.org/wiki/Autonomous_car)
* [IEEE Xplore articles](http://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=autonomous%20car)
* [UAV](https://en.wikipedia.org/wiki/Unmanned_aerial_vehicle)

### Posible Headers

* Intro (100)
* Autonomous Vehicles: The Past & The Present (500)
	* early design
	* historys influence
	* present day equivalent
* Who Is Currently Working on AVs? (500)
	* current companies working on av's
	* current AV market
	* prototypes
* Autonomous Pathing system (500)
	* How dose the AV see
	* How will the AV view pedestreans
	* security problems
* Planes Without Pilots (500)
	* Drones and there influence
	* Passenger planes
	* futur implacations
* Public implications (500)
	* public perception
	* public integration
	* futur implacations
* Military grade AV's (500)
	* Military usage
	* past uses of military av's
	* futur implacations
* Conclusion (100)