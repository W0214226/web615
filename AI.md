# Natural Interaction with Artificial Intelligence #

### Intro ###

“Intelligence is the ability to adapt to change” – Stephen Hawking

The first developments in speech recognition predate the invention of the modern computer by more than 50 years. Alexander Graham Bell was inspired to experiment in transmitting speech by his wife, who was deaf. He initially hoped to create a device that would transform audible words into a visible picture that a deaf person could interpret. He did produce spectrographic images of sounds, but his wife was unable to decipher them. That line of research eventually led to his invention of the telephone. By the end of this document I hope to have informed you on how computer science is working towards a means of communicating with intelligent AI using voice recognition software. I will cover what constitutes an AI, strides in the voice recognition industry, innovations from companies like Microsoft and Apple, how computer learn to understand us using NPL and how this technology might look in the near future.

### What is AI ###

For starters let’s talk about what an AI actually is. Artificial intelligence is the intelligence exhibited by machines or software. Major AI researchers and textbooks define this field as "the study and design of intelligent agents", in which an intelligent agent is a system that perceives its environment and takes actions that maximize its chances of success. John McCarthy, who coined the term in 1955, defines it as "the science and engineering of making intelligent machines". Artificial intelligence is a branch of computer science that aims to create intelligent machines. It has become an essential part of the technology industry. Research associated with artificial intelligence is highly technical and specialized. The core problems of artificial intelligence include programming computers for certain traits such as:

* Knowledge
* Reasoning
* Problem solving
* Perception
* Learning
* Planning
* Ability to manipulate and move objects

Knowledge engineering is a core part of AI research. Machines can often act and react like humans only if they have abundant information relating to the world. Artificial intelligence must have access to objects, categories, properties and relations between all of them to implement knowledge engineering. Initiating common sense, reasoning and problem-solving power in machines is a difficult and tedious approach. 

Machine learning is another core part of AI. Learning without any kind of supervision requires an ability to identify patterns in streams of inputs, whereas learning with adequate supervision involves classification and numerical regressions. Classification determines the category an object belongs to and regression deals with obtaining a set of numerical input or output examples, thereby discovering functions enabling the generation of suitable outputs from respective inputs. Mathematical analysis of machine learning algorithms and their performance is a well-defined branch of theoretical computer science and often referred to as computational learning theory. Machine perception deals with the capability to use sensory inputs to deduce the different aspects of the world, while computer vision is the power to analyze visual inputs with few sub-problems such as facial, object and speech recognition.

But is true artificial intelligence possible? Can a machine solve any problem that a human being can solve using intelligence? Or are there hard limits to what a machine can accomplish? To this day scientist’s debate over what constitutes intelligence and what constitutes an advanced algorithm, the problem is that we cannot yet characterize in general what kinds of computational procedures we want to call intelligent. 

### Voice recognition ###

The first speech recognition systems could understand only digits. (Given the complexity of human language, it makes sense that inventors and engineers first focused on numbers.) Bell Laboratories designed in 1952 the "Audrey" system, which recognized digits spoken by a single voice. Ten years later, IBM demonstrated at the 1962 World's Fair its "Shoebox" machine, which could understand 16 words spoken in English. Labs in the United States, Japan, England, and the Soviet Union developed other hardware dedicated to recognizing spoken sounds, expanding speech recognition technology to support four vowels and nine consonants. They may not sound like much, but these first efforts were an impressive start, especially when you consider how primitive computers themselves were at the time.

Speech recognition technology made major strides in the 1970s, thanks to interest and funding from the U.S. Department of Defense. The DoD's DARPA Speech Understanding Research (SUR) program, from 1971 to 1976, was one of the largest of its kind in the history of speech recognition, and among other things it was responsible for Carnegie Mellon's "Harpy" speech-understanding system that could understand 1011 words. The '70s also marked a few other important milestones in speech recognition technology, including the founding of the first commercial speech recognition company, Threshold Technology, as well as Bell Laboratories' introduction of a system that could interpret multiple people's voices.

Over the next decade, speech recognition vocabulary jumped from about a few hundred words to several thousand words, and had the potential to recognize an unlimited number of words. One major reason was a new statistical method known as the hidden Markov model. Rather than simply using templates for words and looking for sound patterns, HMM considered the probability of unknown sounds' being words. Equipped with this expanded vocabulary, speech recognition started to work its way into commercial applications for business and specialized industry (for instance, medical use). It even entered the home, in the form of Worlds of Wonder's Julie doll (1987), which children could train to respond to their voice. In the '90s, computers with faster processors finally arrived, and speech recognition software became viable for ordinary people. In 1990, Dragon launched the first consumer speech recognition product, Dragon Dictate, for an incredible price of $9000. Seven years later, the much-improved Dragon NaturallySpeaking arrived. The application recognized continuous speech, so you could speak at about 100 words per minute. However, you had to train the program for 45 minutes, and it was still expensive at $695. 

Until Google Comes along by 2001, computer speech recognition had topped out at 80 percent accuracy, and, near the end of the decade, the technology’s progress seemed to be stalled. Speech recognition technology development began to edge back into the forefront with one major event: the arrival of the Google Voice Search app for the iPhone. The impact of Google’s app is significant for two reasons. First, cell phones and other mobile devices are ideal vehicles for speech recognition, as the desire to replace their tiny on-screen keyboards serves as an incentive to develop better, alternative input methods. Second, Google had the ability to offload the processing for its app to its cloud data centers, harnessing all that computing power to perform the large-scale data analysis necessary to make matches between the user’s words and the enormous number of human-speech examples it gathered. 

In short, the bottleneck with speech recognition has always been the availability of data, and the ability to process it efficiently. Google's app adds, to its analysis, the data from billions of search queries, to better predict what you're probably saying. In 2010, Google added "personalized recognition" to Voice Search on Android phones, so that the software could record users' voice searches and produce a more accurate speech model. The company also added Voice Search to its Chrome browser in mid-2011. Google's English Voice Search system now incorporates 230 billion words from actual user queries.

### Voice recognition technologies ###

In recent years the demand for voice recognition devices have skyrocketed and now new innovative products are being created every day that make it possible for the everyman to enjoy products once thought to be only dream. Technologies have been created to allow you to speak to almost every appliance in your life:

* Alarm clocks
* Watches
* Televisions
* Cars
* Lights
* Vacuum cleaners
* Motorbike helmets

Smart cars are a common site these days, but what if you could have all the functionality of a smart car on your head. A device created by Marcus Weller, The Skully Smart Helmet is aimed to make motorcycle riding safer. The helmet is equipped with a heads-up display system with GPS navigation, Bluetooth connectivity, a full 180-degree rear view camera, and it’s all voice-controlled. Just like the smart car, in my opinion, this helmet or others like it will be a common site amongst motercyclers.

For the purposes of this paper I viewed the movie HER were a lonely writer develops an unlikely relationship with his newly purchased operating system that's designed to meet his every need. Though not as impressive as the technology displayed in the movie cubic may just be the next best thing. Cubic Robotics, a technology company based out of Palo Alto, California, has been hard at work for years to create Cubic: an artificially intelligent personal assistant that can connect to all of the tech in our lives. And it isn’t restricted to only our smartphones. Two hardware platforms, the “home cube” and the “power badge”, allows you to have your personal assistant both at home and on the go. And it isn’t just a software app that spits out canned answers to trivia questions or offers directions. Cubic can connect with and operate all the devices, apps and services in your life like the ones listed above. Forget to turn off the lights in your house when you left for work this morning? Cubic will turn them off remotely. Want the temperature to be automatically lowered each night before you turn in for bed? Cubic will learn your needs and take care of them. And thanks to Cubic’s developer-friendly, open API, there are tons of opportunities for integration with gadgets and services yet to come. Cubic is set for a public release in early January or February of 2016. Siri, GoogleNow, Cortana, AmazonEcho, and now Cubic, these are just some of the first steps towards the world depicted in HER and other sifi media.

#### DARPA

Not all voice recognition AI is created to meet the comforts of the average citizen. The Defense Advanced Research Projects Agency (DARPA) is an agency of the U.S. Department of Defense responsible for the development of emerging technologies for use by the military. The Pentagon could be able to listen in on voice communications in difficult environments and then quickly translate and transcribe them for use by intelligence analysts and then quickly translate and transcribe them for use by intelligence analysts and combat troops.

DARPA documents show it is continuing the next two stages of its Robust Automatic Transcription of Speech program (RATS), which is aimed at separating speech from background noise, determining which language is being spoken and then isolating key words from that speech for analysis. The Air Force, DARPA says, is testing the third phase of the program in the field now, while "the research division of a government agency will be testing the speech activity detection algorithm to incorporate into their platform." References to "a government agency" usually refer to a part of the intelligence community, such as the CIA or National Security Agency.

So far, DARPA has spent $13 million on RATS. It now wants to spend another $2.4 million, contract records show, to make the final push to make the system operational by the Air Force as early as this year but by 2017. Although an amassing achievement RATS dose raise some concerns as other agencies, particularly those in the intelligence community, will also use the system once it's operational. The fact that smartphones, certain TV’s, and a wide variety of other devices are actually functioning microphones in a surveillance grid, RATS would be able to decipher and translate any conversation on the planet. But just because it’s capable of that feat, doesn’t mean that’s what I will be used for.

### Conclusion ###

From a device that could only understand numbers being spoken by a single person, To a Cube shaped AI that you could have a conversation with. The speech recognition AI’s time has come, and you can expect plenty more apps in the future. These apps will not only let you control your PC by voice or convert voice to text, they'll also support multiple languages, offer assorted speaker voices for you to choose from, and integrate into every part of your mobile devices. As everyone starts becoming more comfortable speaking aloud to their mobile gadgets, speech recognition technology will likely spill over into other types of devices. At the rate we will reach the impressive feats of communication with artificial intelligence displayed only in works of fiction in our everyday lives in no time.

#### References

* [What is AI](http://www-formal.stanford.edu/jmc/whatisai/)
* [DARPA GALE](http://www.speech.sri.com/projects/GALE/)
* [DARPA RATS](https://catalog.ldc.upenn.edu/LDC2015S02)
* [DARPA RATS problem](http://www.activistpost.com/2015/02/darpa-spending-millions-on-rats-voice.html)
* [World Academy of Science](http://waset.org/publications/10001552/advances-in-artificial-intelligence-using-speech-recognition)
* Wikipedia
	* [NPL](https://en.wikipedia.org/wiki/Natural_language_processing)
	* [Speech recognition](https://en.wikipedia.org/wiki/Speech_recognition)
	* [AI](https://en.wikipedia.org/wiki/Artificial_intelligence)
	* [Beam search](https://en.wikipedia.org/wiki/Beam_search)
	* [Dragon dictate](https://en.wikipedia.org/wiki/DragonDictate)
* [Microsoft Tay](http://readwrite.com/2016/03/28/went-wrong-microsofts-tay-ai/)
* [The Future of Speech Recognition](http://electronics.howstuffworks.com/gadgets/high-tech-gadgets/speech-recognition4.htm)
* [Automatic Speech Recognition—A Brief History of the Technology](http://www.idi.ntnu.no/~gamback/teaching/TDT4275/literature/juang_rabiner04.pdf)
* [cubic](https://www.indiegogo.com/projects/cubic-your-personal-ai-with-personality--2#/)
* [huffington post cubic](http://www.huffingtonpost.com/natalie-kalin/move-over-siri-cubic-robo_b_7895278.html)
* [her](http://www.imdb.com/title/tt1798709/)
